# Electrónica de potencia - Facultad de Ingeniería - UNLP

## Archivos de simulación SPICE

En este respositorio encontrarán una serie de proyectos de **KiCad** para realizar simulaciones de SPICE estructurados en directorios de acuerdo al contenido de la asignatura:

1. Convertidores CA/CC
2. Convertidores CC/CC
3. Convertidores CC/CA
4. Accionamientos de corriente continua
5. Accionamientos de corriente alterna

## Instrucciones para la ejecución de las simulaciones

Los circuitos puede abrirse directamente en Kicad. Se sugiere utilizar a partir de la versión 8.0. El directorio `lib` contiene las bibliotecas de *símbolos* y de *modelos* de componentes de SPICE necesarios para ejecutar algunas simulaciones. Hay que asegurar que este recurso sea accesible desde el directorio raíz de los proyecto que lo requieran.

### Linux
En linux ya existen *symlinks* para ejcutar las simulaciones sin hacer modificación alguna

### Windows
Pueden ejecutar el script `setup.exe` que hace una copia de `lib` en los proyectos que lo requieran.
